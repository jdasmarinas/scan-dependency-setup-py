from setuptools import find_packages, setup

VERSION = '1.0.0.0'

setup(
    name='foobar',
    version=VERSION,
    description='Foobar',
    url='https://foo.bar',
    author='Test',
    author_email='Test@test.com',
    packages=find_packages(exclude=['tests*']),
    include_package_data=True,
    install_requires=[
        'testpackageunique',
        'requests'
    ]
)
